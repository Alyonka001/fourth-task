package geometry;

public abstract class Figures {
	double storona;
	
	public Figures() {
	storona = 50;
	}
	
	public double vozvrat() {
		return storona;
	}
	
	public abstract double empty();
}